const express = require('express')
const QuoteService = require('../services/quote.service')

const router = express.Router();

router.post(`/`, (req, res, next) => {
    return QuoteService.addQuote(req.body)
        .then(value => {
            res.send(value)
        })
        .catch(reason => {
            console.error(reason)
            next(reason);
        })
})
router.put(`/`, (req, res, next) => {
    return QuoteService.updateQuote(req.body)
        .then(value => {
            res.send(value)
        })
        .catch(reason => {
            console.error(reason)
            next(reason);
        })
})

router.get(`/`, (req, res, next) => {
    return QuoteService.findAll()
        .then(value => {
            res.send(value)
        })
        .catch(reason => {
            console.error(reason)
            next(reason);
        })
})


router.get(`/:id`, (req, res, next) => {
    return QuoteService.findById(req.params.id)
        .then(value => {
            res.send(value)
        })
        .catch(reason => {
            console.error(reason)
            next(reason);
        })
})

router.get(`/company/:id`, (req, res, next) => {
    return QuoteService.findByCompanyId(req.params.id)
        .then(value => {
            res.send(value)
        })
        .catch(reason => {
            console.error(reason)
            next(reason);
        })
})

router.delete(`/:id`, (req, res, next) => {
    return QuoteService.deleteById(req.params.id)
        .then(value => {
            res.send(value)
        })
        .catch(reason => {
            console.error(reason)
            next(reason);
        })
})

module.exports = router