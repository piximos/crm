const express = require('express') // imports the express app for its dependencies
const CompanyService = require('../services/company.service') // imports the company service

const router = express.Router(); // creates an express router

router.post(`/`, (req, res, next) => { // defines a 'POST' route
    return CompanyService.addCompany(req.body) // calls the 'addCompany' method and passes it the body of the request that should contain the company data
        .then(value => {
            res.send(value) // send the newly created data
        })
        .catch(reason => {
            console.error(reason)
            next(reason); // pass the error to the errors handler middleware
        })
})
router.put(`/`, (req, res, next) => {
    return CompanyService.updateCompany(req.body)
        .then(value => {
            res.send(value)
        })
        .catch(reason => {
            console.error(reason)
            next(reason);
        })
})

router.get(`/`, (req, res, next) => {
    return CompanyService.findAll()
        .then(value => {
            res.send(value)
        })
        .catch(reason => {
            console.error(reason)
            next(reason);
        })
})


router.get(`/:id`, (req, res, next) => {
    return CompanyService.findById(req.params.id)
        .then(value => {
            res.send(value)
        })
        .catch(reason => {
            console.error(reason)
            next(reason);
        })
})

router.delete(`/:id`, (req, res, next) => {
    return CompanyService.deleteById(req.params.id)
        .then(value => {
            res.send(value)
        })
        .catch(reason => {
            console.error(reason)
            next(reason);
        })
})

module.exports = router // export the router