const express = require('express')
const ItemService = require('../services/item.service')

const router = express.Router();

router.post(`/`, (req, res, next) => {
    return ItemService.addItem(req.body)
        .then(value => {
            res.send(value)
        })
        .catch(reason => {
            console.error(reason)
            next(reason);
        })
})
router.put(`/`, (req, res, next) => {
    return ItemService.updateItem(req.body)
        .then(value => {
            res.send(value)
        })
        .catch(reason => {
            console.error(reason)
            next(reason);
        })
})

router.get(`/`, (req, res, next) => {
    return ItemService.findAll()
        .then(value => {
            res.send(value)
        })
        .catch(reason => {
            console.error(reason)
            next(reason);
        })
})


router.get(`/:id`, (req, res, next) => {
    return ItemService.findById(req.params.id)
        .then(value => {
            res.send(value)
        })
        .catch(reason => {
            console.error(reason)
            next(reason);
        })
})

router.get(`/quote/:id`, (req, res, next) => {
    return ItemService.findByQuoteId(req.params.id)
        .then(value => {
            res.send(value)
        })
        .catch(reason => {
            console.error(reason)
            next(reason);
        })
})

router.delete(`/:id`, (req, res, next) => {
    return ItemService.deleteById(req.params.id)
        .then(value => {
            res.send(value)
        })
        .catch(reason => {
            console.error(reason)
            next(reason);
        })
})

module.exports = router