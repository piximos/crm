const express = require('express')
const ContactService = require('../services/contact.service')

const router = express.Router();

router.post(`/`, (req, res, next) => {
    return ContactService.addContact(req.body)
        .then(value => {
            res.send(value)
        })
        .catch(reason => {
            console.error(reason)
            next(reason);
        })
})
router.put(`/`, (req, res, next) => {
    return ContactService.updateContact(req.body)
        .then(value => {
            res.send(value)
        })
        .catch(reason => {
            console.error(reason)
            next(reason);
        })
})

router.get(`/`, (req, res, next) => {
    return ContactService.findAll()
        .then(value => {
            res.send(value)
        })
        .catch(reason => {
            console.error(reason)
            next(reason);
        })
})


router.get(`/:id`, (req, res, next) => {
    return ContactService.findById(req.params.id)
        .then(value => {
            res.send(value)
        })
        .catch(reason => {
            console.error(reason)
            next(reason);
        })
})

router.get(`/company/:id`, (req, res, next) => {
    return ContactService.findByCompanyId(req.params.id)
        .then(value => {
            res.send(value)
        })
        .catch(reason => {
            console.error(reason)
            next(reason);
        })
})

router.delete(`/:id`, (req, res, next) => {
    return ContactService.deleteById(req.params.id)
        .then(value => {
            res.send(value)
        })
        .catch(reason => {
            console.error(reason)
            next(reason);
        })
})

module.exports = router