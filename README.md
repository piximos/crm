# CRM Project

Under the root of the project, you will find a post man collection that will allow you to communicate with the different
APIs.

## Launching the project

To launch the application in development mode, run the following command :

```shell
npm i
npm run start
```

To launch the application in production mode, run the following command :

```shell
npm i --prod
node index.js
```


## Project Structure

The project has the following tree :

```
app/
├── api
│   ├── company.api.js
│   ├── contact.api.js
│   ├── item.api.js
│   └── quote.api.js
├── config
│   ├── api.js
│   └── database.js
├── Dockerfile
├── index.js
├── package.json
├── package-lock.json
├── schemas
│   ├── company.schema.js
│   ├── contact.schema.js
│   ├── item.schema.js
│   └── quote.schema.js
└── services
    ├── company.service.js
    ├── contact.service.js
    ├── item.service.js
    └── quote.service.js
```

Under the `api` folder you will find the CRUD routes definition of each resource. 

Under the `config` folder you will find the configuration of the API and the database connection. 

The `index.js` file is the main app file that launches  the API and connects to the database. 

Under the `shcema` folder you will find the MongoDB schema definition of each
resource. 

Under the `services` folder you will find the service of each resource.