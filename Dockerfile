FROM node:14

WORKDIR /app
COPY . /app
RUN npm i --prod

ENTRYPOINT ["node", "/app/index.js"]
