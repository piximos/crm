const QuoteModel = require('../schemas/quote.schema')
const CompanyService = require('./company.service')

class QuoteService {
    addQuote(quoteData) {
        this.validateQuoteData(quoteData)
        return CompanyService.findById(quoteData.companyId).then(() => {
            const quote = new QuoteModel(quoteData)
            return quote.save().then(savedQuote => {
                return savedQuote;
            }).catch(reason => {
                throw reason;
            })

        })
            .catch(reason => {
                throw reason;
            })
    }

    updateQuote(quoteData) {
        this.validateQuoteData(quoteData, true)
        return CompanyService.findById(quoteData.companyId).then(() => {
            let quote = quoteData;
            delete quote._id;
            return QuoteModel.findOneAndUpdate(quoteData._id, quote, {
                upsert: false,
                new: true,
            }).then((savedQuote) => {
                return savedQuote;
            }).catch(reason => {
                throw reason;
            })

        })
            .catch(reason => {
                throw reason;
            })
    }

    findAll() {
        return QuoteModel.find().then((quote) => {
            return quote;
        })
            .catch(reason => {
                throw reason;
            })
    }

    findById(quoteId) {
        return QuoteModel.find({_id: quoteId}).then((quote) => {
            if (!quote) {
                throw {message: "Quote not found.", code: 404}
            }
            return quote;
        })
            .catch(reason => {
                throw reason;
            })
    }

    findByCompanyId(companyId) {
        return QuoteModel.find({companyId: companyId}).then((quote) => {
            if (!quote) {
                throw {message: "Quote not found.", code: 404}
            }
            return quote;
        })
            .catch(reason => {
                throw reason;
            })
    }

    deleteById(quoteId) {
        return QuoteModel.findOneAndDelete({_id: quoteId}).then((quote) => {
            return quote;
        })
            .catch(reason => {
                throw reason;
            })
    }

    validateQuoteData(quoteData, update = false) {
        if (update && !quoteData._id) {
            throw {message: "Missing '_id' field.", code: 400}
        }
        if (!quoteData.client) {
            throw {message: "Missing 'client' field.", code: 400}
        }
        if (!quoteData.companyId) {
            throw {message: "Missing 'companyId' field.", code: 400}
        }
        if (!quoteData.total) {
            throw {message: "Missing 'total' field.", code: 400}
        }
        if (!quoteData.reduction) {
            throw {message: "Missing 'reduction' field.", code: 400}
        }
    }
}

module.exports = new QuoteService();