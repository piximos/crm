const ItemModel = require('../schemas/item.schema')
const QuoteService = require('./quote.service')

class ItemService {
    addItem(itemData) {
        this.validateItemData(itemData)
        return QuoteService.findById(itemData.quoteId).then(() => {
            const item = new ItemModel(itemData)
            return item.save().then(savedItem => {
                return savedItem;
            }).catch(reason => {
                throw reason;
            })

        })
            .catch(reason => {
                throw reason;
            })
    }

    updateItem(itemData) {
        this.validateItemData(itemData, true)
        return QuoteService.findById(itemData.quoteId).then(() => {
            let item = itemData;
            delete item._id;
            return ItemModel.findOneAndUpdate(itemData._id, item, {
                upsert: false,
                new: true,
            }).then((savedItem) => {
                return savedItem;
            }).catch(reason => {
                throw reason;
            })

        })
            .catch(reason => {
                throw reason;
            })
    }

    findAll() {
        return ItemModel.find().then((item) => {
            return item;
        })
            .catch(reason => {
                throw reason;
            })
    }

    findById(itemId) {
        return ItemModel.findById(itemId).then((item) => {
            if (!item) {
                throw {message: "Item not found.", code: 404}
            }
            return item;
        })
            .catch(reason => {
                throw reason;
            })
    }

    findByQuoteId(quoteId) {
        return ItemModel.find({quoteId: quoteId}).then((item) => {
            if (!item) {
                throw {message: "Item not found.", code: 404}
            }
            return item;
        })
            .catch(reason => {
                throw reason;
            })
    }

    deleteById(itemId) {
        return ItemModel.findOneAndDelete({_id: itemId}).then((item) => {
            return item;
        })
            .catch(reason => {
                throw reason;
            })
    }

    validateItemData(itemData, update = false) {
        if (update && !itemData._id) {
            throw {message: "Missing '_id' field.", code: 400}
        }
        if (!itemData.name) {
            throw {message: "Missing 'name' field.", code: 400}
        }
        if (!itemData.description) {
            throw {message: "Missing 'description' field.", code: 400}
        }
        if (!itemData.price) {
            throw {message: "Missing 'price' field.", code: 400}
        }
        if (!itemData.quantity) {
            throw {message: "Missing 'quantity' field.", code: 400}
        }
        if (!itemData.quoteId) {
            throw {message: "Missing 'quoteId' field.", code: 400}
        }
    }
}

module.exports = new ItemService();