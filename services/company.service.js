const CompanyModel = require('../schemas/company.schema')

class CompanyService {
    addCompany(companyData) {
        this.validateCompanyData(companyData) // Validate the company's json data that is passed to the service from request body
        const company = new CompanyModel(companyData) // Transforms the company json data to mongodb-ready data
        return company.save().then(savedCompany => { // Saves the data to the database
            return savedCompany; // Returns the saved data (along its _id) to the user
        })
            .catch(reason => {
                throw reason; // In case of an error, return its value to be passed on the the errors handler middleware
            })
    }

    updateCompany(companyData) {
        this.validateCompanyData(companyData, true) // Validate the company's json data that is passed to the service from request body
        let company = companyData; // Copy the company data to a new variable
        delete company._id; // Remove the company ID to not have conflicts with the update
        return CompanyModel.findOneAndUpdate(companyData._id, company, {
            upsert: false, // If a company does not exist with the same '_id', do not create new data
            new: true, // Return the updated data in response
        }).then((savedCompany) => {
            return savedCompany;
        })
            .catch(reason => {
                throw reason;
            })
    }

    findAll() {
        return CompanyModel.find().then((company) => { // find all data by not passing a query
            return company;
        })
            .catch(reason => {
                throw reason;
            })
    }

    findById(companyId) {
        return CompanyModel.findById(companyId).then((company) => {
            if (!company) { // return a 404 error if the company value is null or undefined
                throw {message: "Company not found.", code: 404}
            }
            return company;
        })
            .catch(reason => {
                throw reason;
            })
    }

    deleteById(companyId) {
        return CompanyModel.findOneAndDelete({_id: companyId}).then((company) => {
            return company;
        })
            .catch(reason => {
                throw reason;
            })
    }

    validateCompanyData(companyData, update = false) {
        if (update && !companyData._id) { // if the data is being validated for an update operation, validate that the _id is set otherwise return a 400 error (bad request)
            throw {message: "Missing '_id' field.", code: 400}
        }
        if (!companyData.address) { // validate that the 'address' field is set otherwise return a 400 error (bad request)
            throw {message: "Missing 'address' field.", code: 400}
        }
        if (!companyData.zipcode) { // validate that the 'zipcode' field is set otherwise return a 400 error (bad request)
            throw {message: "Missing 'zipcode' field.", code: 400}
        }
    }
}

module.exports = new CompanyService();