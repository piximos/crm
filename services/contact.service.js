const ContactModel = require('../schemas/contact.schema')
const CompanyService = require('./company.service')

class ContactService {
    addContact(contactData) {
        this.validateContactData(contactData)
        return CompanyService.findById(contactData.companyId).then(() => {
            const contact = new ContactModel(contactData)
            return contact.save().then(savedContact => {
                return savedContact;
            }).catch(reason => {
                throw reason;
            })

        })
            .catch(reason => {
                throw reason;
            })
    }

    updateContact(contactData) {
        this.validateContactData(contactData, true)
        return CompanyService.findById(contactData.companyId).then(() => {
            let contact = contactData;
            delete contact._id;
            return ContactModel.findOneAndUpdate(contactData._id, contact, {
                upsert: false,
                new: true,
            }).then((savedContact) => {
                return savedContact;
            }).catch(reason => {
                throw reason;
            })

        })
            .catch(reason => {
                throw reason;
            })
    }

    findAll() {
        return ContactModel.find().then((contact) => {
            return contact;
        })
            .catch(reason => {
                throw reason;
            })
    }

    findById(contactId) {
        return ContactModel.find({_id: contactId}).then((contact) => {
            if (!contact) {
                throw {message: "Contact not found.", code: 404}
            }
            return contact;
        })
            .catch(reason => {
                throw reason;
            })
    }

    findByCompanyId(companyId) {
        return ContactModel.find({companyId: companyId}).then((contact) => {
            if (!contact) {
                throw {message: "Contact not found.", code: 404}
            }
            return contact;
        })
            .catch(reason => {
                throw reason;
            })
    }

    deleteById(contactId) {
        return ContactModel.findOneAndDelete({_id: contactId}).then((contact) => {
            return contact;
        })
            .catch(reason => {
                throw reason;
            })
    }

    validateContactData(contactData, update = false) {
        if (update && !contactData._id) {
            throw {message: "Missing '_id' field.", code: 400}
        }
        if (!contactData.companyId) {
            throw {message: "Missing 'companyId' field.", code: 400}
        }
        if (!contactData.firstName) {
            throw {message: "Missing 'firstName' field.", code: 400}
        }
        if (!contactData.lastName) {
            throw {message: "Missing 'lastName' field.", code: 400}
        }
        if (!contactData.email) {
            throw {message: "Missing 'email' field.", code: 400}
        }
        if (!contactData.phone) {
            throw {message: "Missing 'phone' field.", code: 400}
        }
    }
}

module.exports = new ContactService();