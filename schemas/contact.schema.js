const mongoose = require('mongoose');

const contactSchema = new mongoose.Schema(
    {
        companyId: {type: String, required: true},
        firstName: {type: String, required: true},
        lastName: {type: String, required: true},
        email: {type: String, required: true},
        phone: {type: String, required: true},
    },
    {
        timestamps: true,
        toObject: {
            versionKey: false,
        },
    },
);

module.exports = mongoose.model('Contacts', contactSchema);
