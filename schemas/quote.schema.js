const mongoose = require('mongoose');

const quoteSchema = new mongoose.Schema(
    {
        client: {type: String, required: true},
        companyId: {type: String, required: true},
        total: {type: Number, required: true},
        reduction: {type: Number, required: true},
        status: {type: Boolean, default: false},
    },
    {
        timestamps: true,
        toObject: {
            versionKey: false,
        },
    },
);

module.exports = mongoose.model('Quotes', quoteSchema);
