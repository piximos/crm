const mongoose = require('mongoose');

const itemSchema = new mongoose.Schema(
    {
        name: {type: String, required: true},
        description: {type: String, required: true},
        price: {type: Number, required: true},
        quantity: {type: Number, required: true},
        quoteId: {type: String, required: true},
    },
    {
        timestamps: true,
        toObject: {
            versionKey: false,
        },
    },
);

module.exports = mongoose.model('Items', itemSchema);
