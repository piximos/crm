const mongoose = require('mongoose');

// Defines the structure of the company data inside the database
const companySchema = new mongoose.Schema(
    {
        name: {type: String, required: true}, // This field is required
        address: {type: String, required: true}, // This field is required
        zipcode: {type: Number, required: true}, // This field is required
        country: {type: String, default: 'Tunisia'}, // This field is not required and its default value is 'Tunisia'
    },
    {
        timestamps: true, // Add 'createdAt' timestamp and 'updatedAt' timestamps to every record
        toObject: {
            versionKey: false,
        },
    },
);

module.exports = mongoose.model('Companies', companySchema); // Creates a model that associate the company schema to the 'Companies' collection and exports it
