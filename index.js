const api = require('./config/api') // Importing API configs

const databaseConnection = require('./config/database') // Importing the method that connects to the database

databaseConnection()
    .then(() => { // If the database connection is successful
        console.info('Successfully connected to database.')
        api.listen(3000, () => { // Starts the application on the port provided by the environment variable if it exists, or else, it uses port 3000
            console.log(`Listening on port 3000`); // If API launch is successful, print a log in console
        })
    })
    .catch(reason => {
        console.error('Could not connect to database.', reason) // If the database connection failed, print the reason behind said failure
    });