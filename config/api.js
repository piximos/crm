const express = require('express') // Import Express (API framework)
const bodyParser = require('body-parser') // Import body-parser for parsing request body
const cors = require('cors'); // import cors for cors management

const CompanyRouter = require('../api/company.api') // Imports the company router
const QuoteRouter = require('../api/quote.api')
const ItemRouter = require('../api/item.api')
const ContactRouter = require('../api/contact.api')

const api = express(); // Creates an new express API
api.use(bodyParser.json()); // Adding the body-parser lib to the newly created express app
api.use(bodyParser.urlencoded({extended: true}));
api.use(cors()); // Adding the cors lib to the newly created express app

// A middleware for sending errors to users
const errorsHandler = (error, req, res, next) => {
    console.error(error)
    const defaultError = {message: error, code: 500}
    res.status(error.code || defaultError.code)
        .send(error || defaultError);
}

api.use('/companies', CompanyRouter); // Adds the company router to the applicaiton
api.use('/quotes', QuoteRouter);
api.use('/items', ItemRouter);
api.use('/contacts', ContactRouter);

api.use(errorsHandler) // Adds the errors handler to the app
module.exports = api; // Exports the application