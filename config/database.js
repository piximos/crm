const mongoose = require('mongoose')

const MONGO_URI='mongodb+srv://crm:hp9ge+WIhpqrQkUP@cluster0.3zfd8.mongodb.net/crmData?retryWrites=true&w=majority'

// Exporting a method that connects to the database
module.exports = databaseConnection = () => {
    // Connecting to database
    return mongoose.connect(MONGO_URI, {useNewUrlParser: true, useUnifiedTopology: true, useFindAndModify: false})
}
